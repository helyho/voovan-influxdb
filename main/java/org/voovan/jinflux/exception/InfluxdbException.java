package org.voovan.jinflux.exception;

/**
 * 类文字命名
 *
 * @author: helyho
 * voovan-Influxdb Framework.
 * WebSite: https://github.com/helyho/voovan-Influxdb
 * Licence: Apache v2 License
 */
public class InfluxdbException extends RuntimeException {
    public InfluxdbException(String message) {
        super(message);
    }

    public InfluxdbException(Throwable cause) {
        super(cause);
    }

    public InfluxdbException(String message, Throwable cause) {
        super(message, cause);
    }
}
