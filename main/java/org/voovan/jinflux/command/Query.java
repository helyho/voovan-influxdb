package org.voovan.jinflux.command;

import org.voovan.http.client.HttpClient;

/**
 * 类文字命名
 *
 * @author: helyho
 * voovan-Influxdb Framework.
 * WebSite: https://github.com/helyho/voovan-Influxdb
 * Licence: Apache v2 License
 */
public class Query {
    private String command;
    private String database;
    private String epoch="ns";
    private Boolean pretty = false;
    private String chunked="false";

    public Query(String command) {
        this.command = command;
    }

    public Query(String command, String epoch) {
        this.command = command;
        this.database = database;
        this.epoch = epoch;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getEpoch() {
        return epoch;
    }

    public void setEpoch(String epoch) {
        this.epoch = epoch;
    }

    public Boolean getPretty() {
        return pretty;
    }

    public void setPretty(Boolean pretty) {
        this.pretty = pretty;
    }

    public String getChunked() {
        return chunked;
    }

    public void setChunked(String chunked) {
        this.chunked = chunked;
    }

    public void initHttpClient(HttpClient httpClient){
        if(database!=null) {
            httpClient.putParameters("db", database);
        }

        if(!epoch.equals("ns")) {
            httpClient.putParameters("epoch", epoch);
        }

        if(pretty) {
            httpClient.putParameters("pretty", "true");
        }

        if(!chunked.equals("false")) {
            httpClient.putParameters("chunked", chunked);
        }
    }

    @Override
    public String toString() {
        return "Query{" +
                "command='" + command + '\'' +
                ", database='" + database + '\'' +
                ", epoch='" + epoch + '\'' +
                ", pretty=" + pretty +
                ", chunked='" + chunked + '\'' +
                '}';
    }
}
