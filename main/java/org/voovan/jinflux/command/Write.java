package org.voovan.jinflux.command;

import org.voovan.Global;
import org.voovan.tools.log.Logger;
import org.voovan.http.client.HttpClient;
import org.voovan.jinflux.Consistency;
import org.voovan.jinflux.model.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 类文字命名
 *
 * @author: helyho
 * voovan-Influxdb Framework.
 * WebSite: https://github.com/helyho/voovan-Influxdb
 * Licence: Apache v2 License
 */
public class Write {
    private String database;
    private Consistency consistency;
    private TimeUnit precision;
    private String retentionPolicy;
    private List<Point> points = new ArrayList<Point>();
    private List<String> lines = new ArrayList<String>();

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public Consistency getConsistency() {
        return consistency;
    }

    public void setConsistency(Consistency consistency) {
        this.consistency = consistency;
    }

    public TimeUnit getPrecision() {
        return precision;
    }

    public void setPrecision(TimeUnit precision) {
        this.precision = precision;
    }

    public String getRetentionPolicy() {
        return retentionPolicy;
    }

    public void setRetentionPolicy(String retentionPolicy) {
        this.retentionPolicy = retentionPolicy;
    }

    public List<Point> getPoints() {
        return points;
    }

    public List<String> getLines() {
        return lines;
    }

    public void initHttpClient(HttpClient httpClient) {
        if(database!=null) {
            httpClient.putParameters("db", database);
        }

        if(consistency!=null) {
            httpClient.putParameters("consistency", consistency.value());
        }

        if(precision!=null) {
            httpClient.putParameters("precision", toTimePrecision(precision));
        }

        if(retentionPolicy!=null) {
            httpClient.putParameters("retentionPolicy", retentionPolicy);
        }

        for(Point point : points) {
            lines.add(point.toLine(precision));
        }

        StringBuilder linesBuffer = new StringBuilder();

        for(String line : lines) {
            linesBuffer.append(line).append("\n");
        }

        if(Global.IS_DEBUG_MODE) {
            Logger.debug(linesBuffer.toString());
        }

        httpClient.setData(linesBuffer.toString().trim());
   }


    private static String toTimePrecision(final TimeUnit timeUnit) {
        switch (timeUnit) {
            case HOURS:
                return "h";
            case MINUTES:
                return "m";
            case SECONDS:
                return "s";
            case MILLISECONDS:
                return "ms";
            case MICROSECONDS:
                return "u";
            case NANOSECONDS:
                return "n";
            default:
                throw new IllegalArgumentException("time precision must be one of:" + timeUnit.name());
        }
    }

    @Override
    public String toString() {
        return "Write{" +
                "database='" + database + '\'' +
                ", consistency=" + consistency +
                ", precision=" + precision +
                ", retentionPolicy='" + retentionPolicy + '\'' +
                ", points=" + points +
                ", lines=" + lines +
                '}';
    }
}
