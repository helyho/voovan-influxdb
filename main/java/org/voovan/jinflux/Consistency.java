package org.voovan.jinflux;

/**
 * 类文字命名
 *
 * @author: helyho
 * voovan-Influxdb Framework.
 * WebSite: https://github.com/helyho/voovan-Influxdb
 * Licence: Apache v2 License
 */
public enum Consistency {
    ALL("all"),
    ANY("any"),
    ONE("one"),
    QUORUM("quorum");
    private final String value;

    private Consistency(final String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }
}
