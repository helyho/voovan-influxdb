package org.voovan.jinflux.model;

import java.util.List;

/**
 * 类文字命名
 *
 * @author: helyho
 * voovan-Influxdb Framework.
 * WebSite: https://github.com/helyho/voovan-Influxdb
 * Licence: Apache v2 License
 */
public class Result {
    private List<Series> series;
    private String error;

    public boolean hasSeries(){
        return series!=null && series.size() > 0;
    }

    public List<Series> getSeries() {
        return series;
    }

    public String getError() {
        return error;
    }
}

