package org.voovan.jinflux.model;

import java.util.List;

/**
 * 类文字命名
 *
 * @author: helyho
 * voovan-Influxdb Framework.
 * WebSite: https://github.com/helyho/voovan-Influxdb
 * Licence: Apache v2 License
 */
public class Series {
    private String name;
    private List<String> columns;
    private List<List<Object>> values;

    public String getName() {
        return name;
    }

    public List<String> getColumns() {
        return columns;
    }

    public List<List<Object>> getValues() {
        return values;
    }
}
