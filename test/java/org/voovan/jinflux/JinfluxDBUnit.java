package org.voovan.jinflux;

import org.voovan.jinflux.command.Write;
import org.voovan.jinflux.model.Point;
import org.voovan.tools.TEnv;
import org.voovan.tools.TObject;
import org.voovan.tools.json.JSON;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

/**
 * 类文字命名
 *
 * @author: helyho
 * voovan-Influxdb Framework.
 * WebSite: https://github.com/helyho/voovan-Influxdb
 * Licence: Apache v2 License
 */
public class JinfluxDBUnit {

    public static void main(String[] args) throws Exception {
        Jinflux jinflux = new Jinflux("http://localhost:8086","root", "root", 5000, "test");
        jinflux.createDataBase();
        System.out.println(jinflux.getAllDataBases());

        jinflux.dropDataBase();
        System.out.println(jinflux.getAllDataBases());

        jinflux.close();

        //========================================================================================================
        jinflux = new Jinflux("http://localhost:8086","root", "root", 5000, "BTC_USDT");
        System.out.println(jinflux.getMeasurement());

        System.out.println(jinflux.getSeries());

        System.out.println(JSON.toJSON(jinflux.select("select * from TradeRecord")));

        //========================================================================================================
        jinflux = new Jinflux("http://localhost:8086","root", "root", 5000, "BTC_USDT");

        Jinflux finalJinflux = jinflux;
        TEnv.measure("select", ()->{
            finalJinflux.select("select * from TradeRecord", TradeRecord.class);
        }, TimeUnit.MILLISECONDS);
        System.out.println(JSON.toJSON(jinflux.select("select * from TradeRecord", TradeRecord.class)));

        //========================================================================================================
        Write write = new Write();
        TEnv.sleep(11);
        TEnv.measure("write", ()->{
            finalJinflux.write("trtest,mytag=1 myfield=90 " + System.currentTimeMillis()/1000, TimeUnit.SECONDS);
        }, TimeUnit.MILLISECONDS);

        TEnv.sleep(1000);

        //=========================================================================================================
        TradeRecord tradeRecord = new TradeRecord();
        tradeRecord.setTimestamp((int) (System.currentTimeMillis()/1000));
        tradeRecord.setAmount(new BigDecimal("0.333"));
        tradeRecord.setPrice(new BigDecimal("0.666"));
        tradeRecord.setValue(new BigDecimal("0.999"));
        Point point = Point.convert(tradeRecord);

        TEnv.sleep(1000);
        TradeRecord tradeRecord1 = new TradeRecord();
        tradeRecord1.setTimestamp((int) (System.currentTimeMillis()/1000));
        tradeRecord1.setAmount(new BigDecimal("0.333"));
        tradeRecord1.setPrice(new BigDecimal("0.666"));
        tradeRecord1.setValue(new BigDecimal("0.999"));
        Point point1 = Point.convert(tradeRecord);

        TEnv.measure("writeMany", ()->{
            finalJinflux.writeMany(TObject.asList(point, point1), TimeUnit.SECONDS);
        }, TimeUnit.MILLISECONDS);


    }
}
