package org.voovan.jinflux;

import org.voovan.jinflux.annotation.Column;
import org.voovan.jinflux.annotation.Measurement;
import org.voovan.jinflux.annotation.Tag;
import org.voovan.jinflux.annotation.Time;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * 成交记录
 *
 * @author: helyho
 * quotations Framework.
 * WebSite: https://github.com/helyho/quotations
 * Licence: Apache v2 License
 */
@Measurement(name="trtest")
public class TradeRecord {
    @Tag
    private BigDecimal price;
    @Column
    private BigDecimal amount;
    @Column
    private BigDecimal value;
    @Time
    private Integer timestamp;

    public TradeRecord() {
    }

    public TradeRecord(BigDecimal price, BigDecimal amount, BigDecimal value, Integer timestamp) {
        this.price = price;
        this.amount = amount;
        this.value = value;
        this.timestamp = timestamp;
    }

    public TradeRecord(List data) {
        this.price  = BigDecimal.valueOf(Double.valueOf(data.get(0).toString()));
        this.amount  = BigDecimal.valueOf(Double.valueOf(data.get(1).toString()));
        this.value  =  BigDecimal.valueOf(Double.valueOf(data.get(2).toString()));;
        this.timestamp = Integer.valueOf(data.get(3).toString());
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TradeRecord)) return false;
        TradeRecord that = (TradeRecord) o;
        return Objects.equals(price, that.price) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(value, that.value) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, amount, value, timestamp);
    }

    public String getTradeLineRecord() {
        timestamp = (int)(System.currentTimeMillis() / 1000);
        long uniqueTime = TimeUnit.NANOSECONDS.convert(timestamp, TimeUnit.SECONDS);
        String lineRecord = "org.voovan.influx.TradeRecord " +
                "amount=" + this.amount        + "," +
                "price=" + this.price          + "," +
                "value=" + this.value          + "," +
                "timestamp=" + this.timestamp  + " " +
                uniqueTime;
        return lineRecord;
    }
}
